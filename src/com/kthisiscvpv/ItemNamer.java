package com.kthisiscvpv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class ItemNamer extends JavaPlugin {

	public void onEnable() {

	}

	public void onDisable() {

	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("item")) {
			if (!(sender instanceof Player))
				sender.sendMessage(ChatColor.RED + "This Command is for Players Only!");
			else {
				Player p = (Player) sender;
				ItemStack item = p.getItemInHand();
				if (item == null || item.getType() == Material.AIR)
					p.sendMessage(ChatColor.RED + "You must be holding an item in your hand!");
				else {
					StringBuilder sB = new StringBuilder();

					sB.append(ChatColor.YELLOW + p.getName() + ChatColor.RED + " is holding " + ChatColor.YELLOW);
					sB.append(item.getAmount() != 1 ? item.getAmount() : (Arrays.asList('a', 'e', 'i', 'o', 'u').contains(Character.toLowerCase(item.getType().name().toCharArray()[0])) ? "an" : "a"));
					sB.append(" " + item.getType().name().toLowerCase().replaceAll("_", " "));
					sB.append(item.getAmount() != 1 ? "s" : "");

					ItemMeta itemM = item.getItemMeta();

					if (itemM != null) {
						if (itemM.hasDisplayName())
							sB.append(ChatColor.RED + " with the name " + ChatColor.YELLOW + itemM.getDisplayName());

						if (itemM.hasLore()) {
							sB.append(ChatColor.RED + " with the lore ");
							for (int i = 0; i < itemM.getLore().size(); i++) {
								if (i > 0) {
									sB.append(ChatColor.RED + ", ");
									if (i == itemM.getLore().size() - 1)
										sB.append("and ");
								}
								sB.append(ChatColor.YELLOW + itemM.getLore().get(i));
							}
						}

						if (itemM.hasEnchants()) {
							List<Enchantment> enchants = new ArrayList<Enchantment>(itemM.getEnchants().keySet());
							sB.append(ChatColor.RED + " with the enchantment" + (enchants.size() != 1 ? "s" : "") + " ");
							for (int i = 0; i < enchants.size(); i++) {
								if (i > 0) {
									sB.append(ChatColor.RED + ", ");
									if (i == enchants.size() - 1)
										sB.append("and ");
								}
								sB.append(ChatColor.YELLOW + enchants.get(i).getName().toLowerCase().replaceAll("_", " ") + " " + itemM.getEnchants().get(enchants.get(i)));
							}
						}
					}

					Bukkit.broadcastMessage(sB.append(ChatColor.RED + ".").toString());
				}
			}
		}
		return false;
	}
}
